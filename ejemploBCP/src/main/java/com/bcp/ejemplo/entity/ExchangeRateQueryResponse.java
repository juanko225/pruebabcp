package com.bcp.ejemplo.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRateQueryResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private ExchangeRateRequestID exchangeRateRequestID;
	private double amount;
	private double exchangeRate;
	private double amountExchangeRate;
	
}
