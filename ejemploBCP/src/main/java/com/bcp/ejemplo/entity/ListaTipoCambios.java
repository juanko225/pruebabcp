package com.bcp.ejemplo.entity;

import java.io.Serializable;
import java.util.List;

public class ListaTipoCambios implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private List<ExchangeRateQuery> lstExchangeRateQuery;

	public List<ExchangeRateQuery> getLstExchangeRateQuery() {
		return lstExchangeRateQuery;
	}

	public void setLstExchangeRateQuery(List<ExchangeRateQuery> lstExchangeRateQuery) {
		this.lstExchangeRateQuery = lstExchangeRateQuery;
	}
	
	
	
}
