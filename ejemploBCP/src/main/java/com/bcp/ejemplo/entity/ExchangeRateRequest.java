package com.bcp.ejemplo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "EXCHANGERATEOPERATION")
public class ExchangeRateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
    private ExchangeRateRequestID exchangeRateRequestID;
	
	@Column(name = "AMOUNT")
	private double amount;
	

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public ExchangeRateRequestID getExchangeRateRequestID() {
		return exchangeRateRequestID;
	}

	public void setExchangeRateRequestID(ExchangeRateRequestID exchangeRateRequestID) {
		this.exchangeRateRequestID = exchangeRateRequestID;
	}


	
	
	

}
