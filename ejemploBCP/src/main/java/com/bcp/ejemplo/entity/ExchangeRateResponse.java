package com.bcp.ejemplo.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;

public class ExchangeRateResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private ExchangeRateRequestID exchangeRateRequestID;
	private double amount;
	private double exchangeRate;
	private double amountExchangeRate;
	
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public double getAmountExchangeRate() {
		return amountExchangeRate;
	}
	public void setAmountExchangeRate(double amountExchangeRate) {
		this.amountExchangeRate = amountExchangeRate;
	}
	public ExchangeRateRequestID getExchangeRateRequestID() {
		return exchangeRateRequestID;
	}
	public void setExchangeRateRequestID(ExchangeRateRequestID exchangeRateRequestID) {
		this.exchangeRateRequestID = exchangeRateRequestID;
	}
	
	
	
}
