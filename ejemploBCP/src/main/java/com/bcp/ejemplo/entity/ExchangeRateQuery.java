package com.bcp.ejemplo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "EXCHANGERATEOPERATION")
public class ExchangeRateQuery implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
    private ExchangeRateRequestID exchangeRateRequestID;
	
	
	
	@Column(name = "EXCHANGERATE", nullable = false)
	private double exchangeRate;
	
    public static BaseWebResponse successNoData() {
        return BaseWebResponse.builder()
                .build();
    }

	



	public ExchangeRateRequestID getExchangeRateRequestID() {
		return exchangeRateRequestID;
	}

	public void setExchangeRateRequestID(ExchangeRateRequestID exchangeRateRequestID) {
		this.exchangeRateRequestID = exchangeRateRequestID;
	}

	public double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	
}
