package com.bcp.ejemplo.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.ejemplo.entity.BaseWebResponse;
import com.bcp.ejemplo.entity.ExchangeRateQuery;
import com.bcp.ejemplo.entity.ExchangeRateQueryResponse;
import com.bcp.ejemplo.entity.ExchangeRateRequest;
import com.bcp.ejemplo.entity.ExchangeRateResponse;
import com.bcp.ejemplo.entity.ListaTipoCambios;
import com.bcp.ejemplo.service.IAmountExchangeRate;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@RestController
@RequestMapping (value="ExchangeRateController")
public class ExchangeRateController {

	private Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IAmountExchangeRate amountExchangeRateService;
	
	
	
	@PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value="/Change"
    ) 
	public Single<ResponseEntity<BaseWebResponse<ExchangeRateResponse>>>  getExchangeRateResponse(@RequestBody ExchangeRateRequest exchangeRateRequest)  {
		
		
		return amountExchangeRateService.getAmountExchangeRate(exchangeRateRequest)
				.subscribeOn(Schedulers.io())
				.map(exchangeRateResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(exchangeRateResponse)));
		
    }
	
	@GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value="/Listar/{limit}/{page}"
    ) 
	public Single<ResponseEntity<BaseWebResponse<List<ExchangeRateQueryResponse>>>>  getLstExchangeRateQueryResponse(@PathVariable int limit, @PathVariable int page)  {		
		
		return amountExchangeRateService.getLstExchangeRateQueryResponse(limit, page)
				.subscribeOn(Schedulers.io())
				.map(exchangeRateQueryResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(exchangeRateQueryResponse)));
		
    }	
	
	@GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value="/ListarTotal"
    ) 
	public Single<ResponseEntity<BaseWebResponse<List<ExchangeRateQueryResponse>>>>  getLstExchangeRateQueryResponse()  {		
		
		return amountExchangeRateService.getLstTotalExchangeRateQueryResponse()
				.subscribeOn(Schedulers.io())
				.map(exchangeRateQueryResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(exchangeRateQueryResponse)));
		
    }	
	
	
//    private List<ExchangeRateQueryResponse> toBookWebResponseList(List<ExchangeRateQueryResponse> bookResponseList) {
//        return bookResponseList
//                .stream()
//                .map(this::toExchangeRateQueryResponse)
//                .collect(Collectors.toList());
//    }
//
//    private ExchangeRateQueryResponse toExchangeRateQueryResponse(ExchangeRateQueryResponse exchangeRateQueryResponse) {
//    	ExchangeRateQueryResponse resultadoExchangeRateQueryResponse = new ExchangeRateQueryResponse();
//        BeanUtils.copyProperties(exchangeRateQueryResponse, resultadoExchangeRateQueryResponse);
//        return resultadoExchangeRateQueryResponse;
//    }	
	
    @PutMapping(            
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/updateExchangeRate"
    )
    public Single<ResponseEntity<BaseWebResponse>> updateBook(@RequestBody ExchangeRateQuery exchangeRateQuery) {
        return amountExchangeRateService.exchangeRateQueryChange(exchangeRateQuery)
                .subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(BaseWebResponse.successNoData()));
    }	
    
    
	@PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            value="/addList"
    ) 
	public Single<ResponseEntity<BaseWebResponse>>  addList(@RequestBody ListaTipoCambios lstexchangeRateRequest)  {
		
		
		return amountExchangeRateService.addList(lstexchangeRateRequest)
				.subscribeOn(Schedulers.io())
				.toSingle(() -> ResponseEntity.ok(BaseWebResponse.successNoData()));
		
    }    
    
}
