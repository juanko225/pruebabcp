package com.bcp.ejemplo.service;
import java.util.List;

import com.bcp.ejemplo.entity.*;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface IAmountExchangeRate {


	
	public Single<ExchangeRateResponse> getAmountExchangeRate(ExchangeRateRequest e);
	public Completable exchangeRateQueryChange(ExchangeRateQuery exchangeRateQuery);
	public Completable addList (ListaTipoCambios lstexchangeRateQuery);
	public Single<List<ExchangeRateQueryResponse>> getLstExchangeRateQueryResponse(int limit, int page);
	public Single<List<ExchangeRateQueryResponse>> getLstTotalExchangeRateQueryResponse();
}
