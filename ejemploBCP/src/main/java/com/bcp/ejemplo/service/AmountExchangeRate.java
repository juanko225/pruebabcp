package com.bcp.ejemplo.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.bcp.ejemplo.dao.IExchangeRateDao;
import com.bcp.ejemplo.entity.ExchangeRateQuery;
import com.bcp.ejemplo.entity.ExchangeRateQueryResponse;
import com.bcp.ejemplo.entity.ExchangeRateRequest;
import com.bcp.ejemplo.entity.ExchangeRateResponse;
import com.bcp.ejemplo.entity.ListaTipoCambios;

import io.reactivex.Completable;
import io.reactivex.Single;
import java.util.stream.Collectors;

@Service
public class AmountExchangeRate implements IAmountExchangeRate{

	@Autowired
	private IExchangeRateDao iExchangeRateDao;
	
	ExchangeRateResponse exRateResponse;
	ExchangeRateQueryResponse exchangeRateQueryResponse;
	
	@Override
	public Single<ExchangeRateResponse> getAmountExchangeRate(ExchangeRateRequest e) {
		exRateResponse = new ExchangeRateResponse();
		return Single.create(singleSubscriber -> {
            Optional<ExchangeRateQuery> optionalExchangeRateResponse = iExchangeRateDao.findById(e.getExchangeRateRequestID());
            if (!optionalExchangeRateResponse.isPresent())
                singleSubscriber.onError(new EntityNotFoundException());
            else {
            	exRateResponse.setExchangeRateRequestID(e.getExchangeRateRequestID());
            	exRateResponse.setAmount(e.getAmount());
            	exRateResponse.setExchangeRate(optionalExchangeRateResponse.get().getExchangeRate());
        		exRateResponse.setAmountExchangeRate(exRateResponse.getAmount()*exRateResponse.getExchangeRate());
                singleSubscriber.onSuccess(exRateResponse);
            }
        });
		
	}
	
    public Single<List<ExchangeRateQueryResponse>> getLstTotalExchangeRateQueryResponse() {
        return lstTotalExchangeRepository()
                .map(this::lstExchangeRateQueryResponse);
    }
    
	//busco la lista de tipos de cambios paginado
	public Single<List<ExchangeRateQuery>> lstTotalExchangeRepository(){
		return Single.create(singleSubscriber -> {
			List<ExchangeRateQuery> lista =    iExchangeRateDao.findAll();
			singleSubscriber.onSuccess(lista);
		});
	}     
	
    public Single<List<ExchangeRateQueryResponse>> getLstExchangeRateQueryResponse(int limit, int page) {
        return lstExchangeRepository(limit, page)
                .map(this::lstExchangeRateQueryResponse);
    }
	
	//busco la lista de tipos de cambios paginado
	public Single<List<ExchangeRateQuery>> lstExchangeRepository(int limit, int page){
		return Single.create(singleSubscriber -> {
			List<ExchangeRateQuery> lista =    iExchangeRateDao.findAll(PageRequest.of(page, limit)).getContent();
			singleSubscriber.onSuccess(lista);
		});
	} 
	
	//mapeo el resultado y los transformo  a un entity response
	public List<ExchangeRateQueryResponse> lstExchangeRateQueryResponse(List<ExchangeRateQuery> lstExchangeRateQuery){
		return lstExchangeRateQuery.stream().map(this::toExchangeRateQueryResponse).collect(Collectors.toList());
	}
	
	//mapeo de entity a response 
	public ExchangeRateQueryResponse toExchangeRateQueryResponse (ExchangeRateQuery exchangeRateQuery) {
		exchangeRateQueryResponse = new ExchangeRateQueryResponse();		
		BeanUtils.copyProperties(exchangeRateQuery, exchangeRateQueryResponse);		
		return exchangeRateQueryResponse;
		
	}
	
	
	
	public Completable exchangeRateQueryChange(ExchangeRateQuery exchangeRateQuery) {
        return updateExchangeRateToRepository(exchangeRateQuery);
    }

    private Completable updateExchangeRateToRepository(ExchangeRateQuery exchangeRateQuery) {
        return Completable.create(completableSubscriber -> {
            Optional<ExchangeRateQuery> optionalExchangeRate = iExchangeRateDao.findById(exchangeRateQuery.getExchangeRateRequestID());
            if (!optionalExchangeRate.isPresent())
                completableSubscriber.onError(new EntityNotFoundException());
            else {
            	ExchangeRateQuery exchangeRate = optionalExchangeRate.get();
            	exchangeRate.setExchangeRate(exchangeRateQuery.getExchangeRate());
            	iExchangeRateDao.save(exchangeRate);
                completableSubscriber.onComplete();
            }
        });
    }
    
    
    public Completable addList (ListaTipoCambios lstexchangeRateQuery) {
    	return Completable.create(completableSubscriber -> {
    		
    		for (ExchangeRateQuery e : lstexchangeRateQuery.getLstExchangeRateQuery()) {
    			iExchangeRateDao.save(e);
			}
    		completableSubscriber.onComplete();    		
        });
    }

}
