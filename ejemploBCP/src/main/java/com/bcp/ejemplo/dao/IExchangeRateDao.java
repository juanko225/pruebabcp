package com.bcp.ejemplo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bcp.ejemplo.entity.ExchangeRateQuery;
import com.bcp.ejemplo.entity.ExchangeRateRequestID;

@Repository
public interface IExchangeRateDao extends JpaRepository<ExchangeRateQuery, ExchangeRateRequestID>{

}
